﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;


public class ZombieAttack : NetworkBehaviour
{

    private float attackRate = 3;
    private float nextAttack;
    private int damage = 10;
    private float minDistance = 2; //min distance before zombie can actually attack
    private float currentDistance;
    private Transform myTransform;
    private ZombieTarget targetScript;

    [SerializeField]
    private Material zombieGreen;
    [SerializeField]
    private Material zombieRed;


    // Use this for initialization
    void Start()
    {
        myTransform = transform;
        targetScript = GetComponent<ZombieTarget>();

        if(isServer)
            StartCoroutine(Attack());
    }

    IEnumerator Attack()
    {
        for (;;) {
            yield return new WaitForSeconds(0.2f);
            CheckIfTargetInRange();
        }
    }

    void CheckIfTargetInRange() {
        if (targetScript.targetTransform != null) {
            currentDistance = Vector3.Distance(targetScript.targetTransform.position, myTransform.position);

            if (currentDistance < minDistance && Time.time > nextAttack) {
                nextAttack = Time.time + attackRate;

                targetScript.targetTransform.GetComponent<PlayerHealth>().DeductHealth(damage);
                StartCoroutine(ChangeZombieMap()); //For the host player
                RpcChangeZombieAppearance();
            }
                          
        }
    }

    IEnumerator ChangeZombieMap() {
        GetComponent<Renderer>().material = zombieRed;
        yield return new WaitForSeconds(attackRate / 2);
        GetComponent<Renderer>().material = zombieGreen;                                                              
    }

    [ClientRpc]
    void RpcChangeZombieAppearance() {
        StartCoroutine(ChangeZombieMap());
    }



}

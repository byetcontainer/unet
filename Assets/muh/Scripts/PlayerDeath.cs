﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PlayerDeath : NetworkBehaviour{

    private PlayerHealth healthScript;
    private Image crossHairImage;

    public override void PreStartClient()
    {
        base.PreStartClient();
        healthScript = GetComponent<PlayerHealth>();
        healthScript.EventDie += DisablePlayer;
    }

    public override void OnStartLocalPlayer()
    {
        crossHairImage = GameObject.Find("crossHairImage").GetComponent<Image>();
    }

    // Use this for initialization
    void Start () {

	}

    public override void OnNetworkDestroy() {
        healthScript.EventDie -= DisablePlayer;
    }



    // Update is called once per frame
    void Update () {
	
	}


    void DisablePlayer() {
        GetComponent<CharacterController>().enabled = false;
        GetComponent<PlayerShoot>().enabled = false;
        GetComponent<BoxCollider>().enabled = false;

        Renderer[] renderers = GetComponentsInChildren<Renderer>();
        foreach (Renderer ren in renderers) {
            ren.enabled = false;
        }

        healthScript.isDead = true;

        if (isLocalPlayer) {
            GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().enabled = false;
            crossHairImage.enabled = false;
            GameObject.Find("GameManager").GetComponent<GameManagerReferences>().respawnButton.SetActive(true);

        }
    }
}

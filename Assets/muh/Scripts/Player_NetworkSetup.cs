﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Player_NetworkSetup : NetworkBehaviour {

    [SerializeField]
    Camera FPSCharacterCamera;
    [SerializeField]
    AudioListener audioListener;


    // Use this for initialization

    public override void OnStartLocalPlayer()
    {
        if (GameObject.Find("SceneCamera").activeSelf)
            GameObject.Find("SceneCamera").SetActive(false);

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        //GetComponent<CharacterController>().enabled = true;
        GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().enabled = true;
        FPSCharacterCamera.enabled = true;
        audioListener.enabled = true;

        Renderer[] rens = GetComponentsInChildren<Renderer>(); //So we don't see our own world model
        foreach (Renderer ren in rens) {
            ren.enabled = false;
        }


        GetComponent<NetworkAnimator>().SetParameterAutoSend(0, true);

    }

    public override void PreStartClient()
    {
        GetComponent<NetworkAnimator>().SetParameterAutoSend(0, true);
    }


}
